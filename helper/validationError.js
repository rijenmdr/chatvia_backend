const { validationResult } = require("express-validator");

//Show Errors For Validation Rules
exports.showValidationErrors = (req, res, callback) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ message: errors.array(), success: false });
  }
};
