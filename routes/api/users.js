const express = require("express");

const userController = require("../../controller/users");
const {
  loginValidator,
  signupValidator,
  emailValidator,
  otpValidator,
  forgotPasswordValidator,
} = require("../../validator/user");

const router = express.Router();

router.post("/login", loginValidator, userController.login);
router.post("/register", signupValidator, userController.signup);
router.post("/get-otp", emailValidator, userController.getOTP);
router.post("/verify-otp", otpValidator, userController.verifyOTP);
router.post(
  "/forgot-password",
  forgotPasswordValidator,
  userController.forgotPassword
);

module.exports = router;
