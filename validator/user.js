const { check } = require("express-validator");

exports.loginValidator = [
  check("email")
    .notEmpty()
    .withMessage("Email is required")
    .isEmail()
    .withMessage("Invalid Email Address"),
  check("password")
    .notEmpty()
    .withMessage("Password is required")
    .matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/)
    .withMessage(
      "Minimum eight characters, at least one letter, one number and one special character"
    ),
];

exports.signupValidator = [
  check("name").notEmpty().trim().escape().withMessage("Name is required"),
  check("email")
    .notEmpty()
    .withMessage("Email is required")
    .isEmail()
    .withMessage("Invalid Email Address"),
  check("password")
    .notEmpty()
    .withMessage("Password is required")
    .matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/)
    .withMessage(
      "Minimum eight characters, at least one letter, one number and one special character"
    ),
];

exports.emailValidator = [
  check("email")
    .notEmpty()
    .withMessage("Email is required")
    .isEmail()
    .withMessage("Invalid Email Address"),
];

exports.otpValidator = [
  check("email")
    .notEmpty()
    .withMessage("Email is required")
    .isEmail()
    .withMessage("Invalid Email Address"),
  check("otp").notEmpty().withMessage("OTP is required"),
];

exports.forgotPasswordValidator = [
  check("otp").notEmpty().withMessage("OTP is required"),
  check("email")
    .notEmpty()
    .withMessage("Email is required")
    .isEmail()
    .withMessage("Invalid Email Address"),
  check("password")
    .notEmpty()
    .withMessage("Password is required")
    .matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/)
    .withMessage(
      "Minimum eight characters, at least one letter, one number and one special character"
    ),
];
