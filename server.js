const express = require("express");
const { createServer } = require("http");
const { Server } = require("socket.io");
const cors = require("cors");
const mongoose = require("mongoose");

const authRoutes = require("./routes/api/users");
const User = require("./models/User");

const onlineUsers = {};

require("dotenv").config();
const port = process.env.PORT || 5000;
const mongoDB = process.env.MONGO_URL;

const app = express();
const httpServer = createServer(app);
const io = new Server(httpServer);

app.use(cors());
app.use(express.urlencoded({ extended: true, limit: "10mb" }));
app.use(express.json({ limit: "100mb" }));
app.use(express.static(__dirname + "/public/uploads"));

//Routes
app.use("/api/users", authRoutes);

//socket connection
io.on("connection", async (socket) => {
  // Get connected user id
  const userId = socket.handshake.query.userId;
  // Set user as online
  onlineUsers[userId] = socket.id;

  //for using socketid on controller
  const newSocketMapping = new SocketMapping({
    user: userId,
    socketId: socket.id,
  });
  await newSocketMapping.save();

  // Notify new user to all online users
  socket.broadcast.emit("init", onlineUsers);
  socket.emit("init", onlineUsers);

  // User disconnected
  socket.on("disconnect", async () => {
    let disconnectedUserId = null;
    // Remove disconnected user from online users
    for (prop in onlineUsers) {
      if (onlineUsers[prop] === socket.id) {
        disconnectedUserId = prop;
        delete onlineUsers[prop];
        break;
      }
    }

    await SocketMapping.findOneAndRemove({ socketId: socket.id });

    if (disconnectedUserId) {
      User.findByIdAndUpdate(
        disconnectedUserId,
        { $set: { last_seen: new Date() } },
        function (err, doc) {
          if (err) console.log(err);
        }
      );
    }

    // Notify disconnected user to all online users
    socket.broadcast.emit("user_disconnected", onlineUsers);
  });

  // Event for start typing
  socket.on("start-typing", (data) => {
    const info = JSON.parse(data);
    socket
      .to(onlineUsers[info.receiver])
      .emit("user-start-typing", { sender: info.sender, roomId: info.roomId });
  });

  // Event for stop typing
  socket.on("stop-typing", (data) => {
    const info = JSON.parse(data);
    socket
      .to(onlineUsers[info.receiver])
      .emit("user-stop-typing", { sender: info.sender, roomId: info.roomId });
  });

  // Event for sending message
  socket.on("send-message", (data) => {
    const info = JSON.parse(data);
    const { sender, roomId, message, receiver } = info;
    socket.to(onlineUsers[receiver]).emit("receive-message", newMessage);
  });

  // socket.emit('message-sent-success', newMessage);
  // .then((msg) => {

  // }).catch((err) => {
  //   socket.emit('message-sent-failed', msg);
  // });
});

//error handling
app.use((error, req, res, next) => {
  console.log("Error Handling Middleware called");
  console.log("Path: ", req.path);
  console.error("Error: ", error?.message);

  return res
    .status(500)
    .send({ message: "Something went wrong. Please try again later." });
});

//db connection
mongoose
  .connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log("Connected to MongoDB"))
  .catch((err) => console.log(err));

httpServer.listen(port, () =>
  console.log(`Server up and running on port ${port} !`)
);
