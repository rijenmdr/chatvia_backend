const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { generateRandomString } = require("../helper/generateRandomString");
const { showValidationErrors } = require("../helper/validationError");

const User = require("../models/User");

module.exports.login = async (req, res, next) => {
  const { email, password } = req.body;

  if (showValidationErrors(req, res)) return;

  try {
    const user = await User.findOne({ email }).select({ otp: 0 });

    if (!user) {
      return res.status(404).json({
        message: "User not found",
      });
    }

    const isMatch = await bcrypt.compare(password, user?.password);

    if (!isMatch) {
      return res.status(400).json({
        message: "Email / Password is incorrect",
      });
    }

    const payload = {
      _id: user?._id,
      name: user?.name,
      email: user?.email,
    };

    const token = await jwt.sign(payload, process.env.SECRET_KEY);

    return res.status(200).json({
      message: "Login Success",
      data: {
        user,
        token,
      },
    });
  } catch (err) {
    next(err);
  }
};

module.exports.signup = async (req, res, next) => {
  const { name, email, password } = req.body;

  if (showValidationErrors(req, res)) return;

  try {
    const user = await User.findOne({ email }).select({ password: 0, otp: 0 });

    if (user) {
      return res.status(400).json({
        message: "Email Already in use",
      });
    }

    const salt = await bcrypt.genSalt();
    const hashedPassword = await bcrypt.hash(password, salt);

    const newUser = new User({
      name,
      email,
      password: hashedPassword,
    });

    await newUser.save();

    return res.status(200).json({
      message: "User created successfully",
    });
  } catch (err) {
    next(err);
  }
};

module.exports.getOTP = async (req, res, next) => {
  const { email } = req.body;

  if (showValidationErrors(req, res)) return;

  try {
    const user = await User.findOne({ email });

    if (!user) {
      return res.status(404).json({
        message: "User not found",
      });
    }

    const otp = generateRandomString(5);

    await User.findOneAndUpdate({ email }, { $set: { otp } });

    return res.status(200).json({
      message: "OTP has be send to your email address",
      otp,
    });
  } catch (err) {
    next(err);
  }
};

module.exports.verifyOTP = async (req, res, next) => {
  const { email, otp } = req.body;

  if (showValidationErrors(req, res)) return;

  try {
    const user = await User.findOne({ email, otp });

    if (!user) {
      return res.status(404).json({
        message: "Invalid OTP",
      });
    }

    return res.status(200).json({
      message: "OTP verified successfully.",
    });
  } catch (err) {
    next(err);
  }
};

module.exports.forgotPassword = async (req, res, next) => {
  const { email, otp, password } = req.body;

  if (showValidationErrors(req, res)) return;

  try {
    const user = await User.findOne({ email, otp });

    if (!user) {
      return res.status(404).json({
        message: "Invalid User Credentials",
      });
    }

    const salt = await bcrypt.genSalt();
    const hashedPassword = await bcrypt.hash(password, salt);

    await User.findOneAndUpdate(
      { email },
      { $set: { password: hashedPassword } }
    );

    return res.status(200).json({
      message: "Password Changed Successfuly",
    });
  } catch (err) {
    next(err);
  }
};
