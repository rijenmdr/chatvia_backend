const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "Please enter your name"],
    },
    profileImg: {
      type: String,
    },
    email: {
      type: String,
      required: [true, "Please enter a email"],
    },
    password: {
      type: String,
      required: [true, "Please enter a password"],
      minlength: [6, "The password should be at least 6 characters long"],
    },
    otp: {
      type: String,
    },
    last_seen: {
      type: Date,
      default: Date.now,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("user", userSchema);
