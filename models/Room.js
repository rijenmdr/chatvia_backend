const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const StaffMessageSchema = new Schema({
  room: {
    type: Schema.Types.ObjectId,
    ref: "staffrooms",
    required: true,
  },
  sender: {
    type: Schema.Types.ObjectId,
    ref: "admin_users",
    required: true,
  },
  message: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
    default: "unread",
  }
}, {
  timestamps: true,
});

module.exports = StaffMessageModel = mongoose.model("staffMessage", StaffMessageSchema);
