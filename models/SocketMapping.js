const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const socketMappingSchema = new mongoose.Schema({
  user: {
    type: Schema.Types.ObjectId,
    refPath: "user",
  },
  socketId: String,
});
module.exports = mongoose.model("socketmapping", socketMappingSchema);
